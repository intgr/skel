# ~/.zshrc

## varia #######################################################################
# if working directory isn't accessible, change directory to home
# shellcheck disable=SC2164
[[ -d . ]] || (echo "$PWD isn't accessible" 1>&2; cd)

## zsh initialization ##########################################################
[[ -d ~/.zsh/completions ]] && fpath=( ~/.zsh/completions "${fpath[@]}" )
autoload -U compinit promptinit
if [[ "$OSTYPE" == darwin* ]]; then
  # Avoid file ownership notice when zsh is installed from Homebrew
  compinit -u
else
  compinit
fi
promptinit
zstyle ':completion::complete:*' use-cache 1

## terminal ####################################################################
#in a tty terminal, set beep length to 0 (don't beep!)
[[ "$TERM" = "linux" ]] && setterm -blength 0
# Disable zsh beep
unsetopt BEEP

## key bindings ################################################################
# Home and End, TERM=rxvt
bindkey "^[[7~" beginning-of-line
bindkey "^[[8~" end-of-line
# Home and End, TERM=screen
bindkey "^[[1~" beginning-of-line
bindkey "^[[4~" end-of-line
# Home and End, TERM=gnome-terminal
bindkey "^[OH" beginning-of-line
bindkey "^[OF" end-of-line
# Home and End, TERM=gnome-terminal (new)
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
# Ctrl-Left and Ctrl-Right, TERM=gnome-terminal
bindkey "^[[1;5D" backward-word
bindkey "^[[1;5C" forward-word
# Ctrl-Left and Ctrl-Right, TERM=IntelliJ IDEA
bindkey "^[[5D" backward-word
bindkey "^[[5C" forward-word
# Ctrl-Left and Ctrl-Right, TERM=rxvt
bindkey "^[Od" backward-word
bindkey "^[Oc" forward-word
# macOS: Alt-Left and Alt-Right, TERM=iterm
bindkey "^[[1;9D" backward-word
bindkey "^[[1;9C" forward-word
# macOS: Shift-Left and Shift-Right, TERM=iterm
bindkey "^[[1;2D" backward-word
bindkey "^[[1;2C" forward-word
# macOS: PageUp, PageDown, TERM=iterm
bindkey "^[[5~" up-line-or-history
bindkey "^[[6~" down-line-or-history
# Delete, TERM=rxvt
bindkey "^[[3~" delete-char
# Alt-Up, Alt-Down, Ctrl-Up, Ctrl-Down
bindkey "^[[1;3A" history-search-backward
bindkey "^[[1;3B" history-search-forward
bindkey "^[[1;5A" history-search-backward
bindkey "^[[1;5B" history-search-forward
# Up, Down -- some configurations use vi-X-line-or-history, which sucks
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search

## environment #################################################################
[[ -f /etc/profile ]] && source /etc/profile

HISTSIZE=2000
HISTFILE=~/.zsh_history
# shellcheck disable=SC2034
SAVEHIST=2000
[[ -z "$LANG$LC_ALL" ]] && export LANG="en_US"

export PATH="$PATH:$HOME/bin"

# Set login timeout for the root user (7200 = 2*3600 = 2h)
[[ "$UID" == 0 ]] && TMOUT=7200

prefer() {
  local var=$1 app
  shift
  for app in "$@"; do
    type "$app" &>/dev/null && export "$var=$app" && return 0
  done
  return 1
}

prefer EDITOR vim vi nano && alias 'vi=$EDITOR'
prefer PAGER lv less && alias 'less=$PAGER'
# shellcheck disable=SC2139
prefer LS eza && alias "ls=$LS"
# shellcheck disable=SC2139
prefer CLIP wl-copy pbcopy && alias "clip=$CLIP"
# shellcheck disable=SC2139
prefer OPEN xdg-open && alias "open=$OPEN"

unset CLIP OPEN

## prompt ######################################################################
if [[ "$TERM" != "dumb" ]]; then
  GREEN=$'%{\e[01;32m%}'
  RED=$'%{\e[01;31m%}'
  NONE=$'%{\e[0;0m%}'
fi

PROMPT="${GREEN}[%(!..${NONE}%n@)${RED}%m${GREEN}]%#${NONE} "
PS2="${GREEN}[${NONE}...${GREEN}]>${NONE} "
# shellcheck disable=SC2034
RPS1="${GREEN}@${NONE}%~"

unset GREEN RED NONE

# Set the term window title
case "$TERM" in
  screen|xterm|rxvt|rxvt-unicode|xterm-256color)
    PROMPT=$'%{\e]0;zsh: %n@%m%# %~\x07%}'$PROMPT
    ;;
esac

## aliases #####################################################################
if [[ "$LS" != "eza" ]]; then
  alias l="ls -Fh"
  alias ll="ls -Fhl"
  alias lla="ls -FhAl"
  alias llt="ls -rFhAlt"
  alias lls="ls -rFhAlS"
else
  alias l="eza"
  alias ll="eza -l"
  alias lla="eza -la"
  alias llt="eza -la --sort=modified"
  alias lls="eza -la --sort=size"
fi
alias cd..='cd ..'
alias x="unset HISTFILE"

alias du1="du -hd1"
alias disasm="objdump -rld"
alias rehash="source ~/.zshrc; rehash"
alias rsync1='rsync -arvP'
alias netstat1='netstat -napt'
alias lsof='lsof -n'
alias dstat1='dstat --cpu --sys --disk --net -Dsda,sdb,sdc,sdd -Neth0'
alias mtr='mtr --curses'
alias grep='grep --color'
alias mpl='mpv --no-ytdl --playlist'
alias cert='openssl x509 -noout -text -fingerprint -sha256 -certopt no_sigdump'
alias csr='openssl req -noout -text --reqopt no_sigdump'
# git
alias co='git checkout'
alias rebi='git rebi'
alias rebu='git rebu'
alias bra='git bra'

# services
if type systemctl &>/dev/null; then
  # shellcheck disable=SC2168
  [[ "$UID" != 0 ]] && local _SUDO='sudo '
  # shellcheck disable=SC2139
  alias restart="${_SUDO}systemctl restart"
  # shellcheck disable=SC2139
  alias reload="${_SUDO}systemctl reload"
  # shellcheck disable=SC2139
  alias stop="${_SUDO}systemctl stop"
  # shellcheck disable=SC2139
  alias start="${_SUDO}systemctl start"
  alias status="systemctl status"
  alias urestart="systemctl --user restart"
  alias ureload="systemctl --user reload"
  alias ustop="systemctl --user stop"
  alias ustart="systemctl --user start"
  alias ustatu="systemctl --user status"
elif type brew &>/dev/null; then
  alias restart="brew services restart"
  alias reload="brew services reload"
  alias stop="brew services stop"
  alias start="brew services start"
  alias status="brew services list"
fi

## functions ###################################################################
f() {
  find . -iname "*$1*"
}
hide() {
  exec "$@" &>/dev/null </dev/null &
}

## local customizations ########################################################
# shellcheck source=.zsh_local
[[ -f ~/.zsh_local ]] && source ~/.zsh_local

## eof #########################################################################
