" ~/.vimrc

" No archaic Vi compatibility
set nocompatible

" Force full colors for "screen" and "xterm"
if &term ==? "xterm" || &term ==? "screen" || &term ==? "rxvt"
  set t_Co=88
endif

" Make sure Vim sets the title behind screen
if &term ==? "screen"
  set t_ts=]0;
  set t_fs=
endif

" Make sure we have a terminal supporting true colors for the scheme.
" t_Co is unset if &term=="builtin_gui"
if &t_Co == "" || &t_Co > 8
  " Pablo color scheme - nice and soft
  colorscheme pablo
endif

" Syntax highlighting
syntax enable
" Filetype-specific detection/plugins/indent on
filetype plugin indent on

" Windows-like intuitive del/backspace keys
set backspace=indent,eol,start
" Highlight search matches
set hlsearch
" Incremental search - show matches while typing
set incsearch
" Case matters in search patterns if uppercase letters are found
set smartcase
" Mouse in normal, visual, insert and commandline modes
set mouse=a
" A real tab character expands into 8 spaces (ANSI)
set tabstop=8
" Indent by 4 spaces
set shiftwidth=4
" Indent by spaces instead of tabs
set expandtab
" Smart tabs - shiftwidth at beginning of lines, tabstop inside lines
set smarttab
" Automatic indentation aids
set autoindent
" Honor Vim/Vi modelines in files
set modeline
" Show longest common match, then complete alphabetically
set wildmode=longest,full
" Pop up a menu when using completion
set wildmenu
" ^U and ^D scroll 4 lines
set scroll=4
" Keep some context (2 lines) around the cursor always
set scrolloff=2
" Don't move cursor to start of line when scrolling
set nostartofline

if has("gui_running")
  if has("gui_win32")
    " Use the ProggySquare font (from http://www.proggyfonts.com/)
    set guifont=ProggySquare:h8:cANSI
  elseif has("gui_gtk2")
    "set guifont=Bistream\ Vera\ Sans\ Mono:h8:cANSI
    "set guifontwide=Bistream\ Vera\ Sans\ Mono:h8:cANSI
    "set guifont=Bistream\ Vera\ Sans\ Mono\ 8
    "set guifontwide=Bistream\ Vera\ Sans\ Mono\ 8
    set guifontset=Bistream\ Vera\ Sans\ Mono\ 8
  endif
endif

" Turn off the useless toolbar in GUI
set guioptions-=T
" Select in GUI copies text immediatelly
set guioptions+=a
" Force a right-aligned scrollbar always
set guioptions+=r

" Set a fancy statusline
set statusline=
set statusline+=%-3.3n\                      " buffer number
set statusline+=%f\                          " filename
set statusline+=%h%m%r%w                     " status flags
set statusline+=\[%{strlen(&ft)?&ft:'none'}] " file type
set statusline+=%=                           " right align remainder
set statusline+=0x%-8B                       " character value
set statusline+=%-14(%l,%c%V%)               " line, character
set statusline+=%<%P                         " file position

" Set the terminal app/GUI title
set title
set titlestring=
if !has("gui_win32")
  set titlestring+=vim:\           " vim tag
  set titlestring+=%{$USER}@       " user@
  set titlestring+=%{hostname()}\  " hostname
endif
set titlestring+=%.32f\          " filename
set titlestring+=%m%r            " status flags

" Remember last cursor position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Disable annoying bell sounds
set visualbell t_vb=

" The semicolon is quicker to type on US keyboards
nmap ; :
nmap \ gqip
cmap w!! %!sudo tee > /dev/null %

" Additional tag mappings
nmap <C-n> :tnext<CR>
nmap <C-p> :tprev<CR>

" Map <C-cursorkeys>
map <Esc>Oa <C-Up>
map <Esc>Ob <C-Down>
map <Esc>Od <C-Left>
map <Esc>Oc <C-Right>

" These don't work in insert mode...
nmap <C-Up>    <C-u>
nmap <C-Down>  <C-d>
nmap <C-Left>  B
nmap <C-Right> W

" In insert mode, hold down control to do movement, cursor keys suck.
" Uhhh, and I have yet to learn to use main-row keys for movement in normal
" mode...
imap <C-h> <Left>
imap <C-j> <Down>
imap <C-k> <Up>
imap <C-l> <Right>

