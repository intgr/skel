#!/bin/sh
# This is a simple script for optimizing PNG files.
#
# When invoked with many arguments, it uses xargs -P to parallelise tasks
# to take advantage of multiple CPU cores.
#
# * optipng     -- tries to find optimal parameters
# * advancecomp -- re-does zlib compression to achieve better efficiency

test -z "$1" && {
  echo "usage: dopng FILENAME [FILENAME ...]"
  exit 1
}

if which optipng >/dev/null; then
  ls -1 "$@" | nice -n10 xargs -d'\n' -t -n1 -P4 -- optipng -o4 -- || exit 1
fi

if which advpng >/dev/null; then
  ls -1 "$@" | nice -n10 xargs -d'\n' -t -n1 -P4 -- advpng -z4 -- || exit 1
  ls -1 "$@" | nice -n10 xargs -d'\n' -t -n1 -P4 -- advdef -z4 -- || exit 1
fi

