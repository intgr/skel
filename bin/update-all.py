#!/usr/bin/env python3
import shlex
import subprocess
from functools import cache
from os import getcwd
from os.path import expanduser
from pathlib import Path
from shutil import which


def cmd(value: str) -> list[str]:
    return shlex.split(value)


def get_home_dir() -> Path:
    return Path(expanduser("~"))


@cache
def has_command(cmd: str):
    exists = which(cmd) is not None
    if not exists:
        print(f"SKIP: {cmd} not found, skipping")
    return exists


def update_directory(dir: Path, auto_recurse: bool = False, run_sweep: bool = False) -> None:
    assert dir.is_dir()

    active_list = dir / 'ACTIVE.list'
    subdirs: list[Path]
    if active_list.exists():
        print(f'Using dirs from {active_list} file')
        sub_dirnames = active_list.open().readlines()
        subdirs = [dir / d.strip() for d in sub_dirnames if d.strip() and not d.strip().startswith('#')]

    elif auto_recurse:
        print(f'Recursing to all subdirectories in {dir}')
        subdirs = list(dir.glob('*/'))

    else:
        subdirs = []

    for subdir in subdirs:
        if not subdir.is_dir():
            print(f"Path {subdir} is not a directory (obsolete ACTIVE.list entry?)!")
            continue

        print()
        # Run sweep in subdirs of $HOME
        update_directory(subdir, run_sweep=dir == get_home_dir())

    if (dir / '.git').exists() and has_command('git'):
        print(f'Updating git in {dir}')
        try:
            subprocess.check_call(cmd('git fetch'), cwd=dir)
            if subprocess.check_output(cmd('git branch --show-current'), cwd=dir) in ('main', 'master'):
                subprocess.check_call(cmd('git pull'), cwd=dir)
        except subprocess.CalledProcessError as err:
            print(f'Error updating {dir}: {err}')

    if (dir / 'Cargo.toml').exists() and has_command('cargo'):
        print(f'Cargo build in {dir}')
        try:
            subprocess.check_call(cmd('cargo fetch'), cwd=dir)
            subprocess.check_call(cmd('cargo build'), cwd=dir)
            subprocess.check_call(cmd('cargo test --no-run'), cwd=dir)
        except subprocess.CalledProcessError as err:
            print(f'Error building {dir}: {err}')

    if run_sweep and has_command('cargo'):
        print(f'Sweeping {dir}')
        subprocess.check_call(cmd('cargo sweep -r --installed'), cwd=dir)


def rustup_update() -> None:
    if not has_command('rustup'):
        return

    print('Updating Rust...')
    try:
        subprocess.check_call(cmd('rustup update'))
        subprocess.check_call(cmd('rustup component add rust-src'))
    except subprocess.CalledProcessError as err:
        print(f'Error updating Rust: {err}')


def main() -> None:
    rustup_update()

    dir = Path(getcwd())
    is_home_dir = dir == get_home_dir()
    update_directory(dir, auto_recurse=not is_home_dir, run_sweep=not is_home_dir)


if __name__ == '__main__':
    main()
