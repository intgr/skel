#!/bin/sh

ORIGIN_NAME=origin

git remote prune "$ORIGIN_NAME"

# Inspiration: https://stackoverflow.com/a/59837633/177663
git branch --sort=committerdate -vv | grep -E " [a-f0-9]{9} \[$ORIGIN_NAME/.*: gone]" | awk '{print $1}' | xargs $PREFIX git branch -D
