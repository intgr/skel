#!/bin/sh

if [[ "$UID" == 0 ]]; then
  pm-suspend
else
  export DISPLAY=:0
  dbus-send --system --print-reply --dest=org.freedesktop.UPower /org/freedesktop/UPower org.freedesktop.UPower.Suspend
fi

