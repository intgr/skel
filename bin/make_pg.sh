#!/bin/sh
set -e

if [ -z "$1" ]; then
  name=pgsql
else
  name="$1"
fi
prefix=/usr/local/$name
echo "Prefix: $prefix"

if [[ ! -f $prefix/env ]]; then
  echo "Creating $prefix/env"
  mkdir -p $prefix
  cat > $prefix/env <<EOF
export PGDATA=$prefix/data
export PGPORT=5499
export PATH=$prefix/bin:\$PATH
test -z "\$_OLD_PS1" && _OLD_PS1="\$PS1"
PS1="$name\$_OLD_PS1"
EOF
fi

. $prefix/env
# Faster shell
test -f /bin/dash && export SHELL=/bin/dash

test -d .git || quit

# Delete all broken links
#find -xtype l -delete

#export CFLAGS="$CFLAGS -g -O0"
if [[ ! -f config.status ]]; then
  echo Configuring...
  ./configure --prefix=$prefix --enable-cassert --enable-debug --with-python --with-llvm >/dev/null
  #./configure --prefix=$prefix --with-python --with-llvm >/dev/null
fi

echo Building...
time make -j16 -s install enable_nls=no INSTALL=install >/dev/null

ulimit -c unlimited

if [ ! -f $PGDATA/PG_VERSION ]; then
  echo "Creating datadir..."
  rm -rf $PGDATA
  initdb --noclean >/dev/null
  cat <<EOF >> $PGDATA/postgresql.conf
fsync = off
full_page_writes = off
max_parallel_workers_per_gather = 16
max_parallel_workers = 32
max_parallel_maintenance_workers = 16
work_mem = 256MB
shared_buffers = 400MB
#logging_collector = on
#shared_preload_libraries = pg_journal
EOF

  echo "CREATE DATABASE $USER" | postgres --single postgres >/dev/null

  pg_ctl start -l $PGDATA/log -w -t3 || tail $PGDATA/log
else
  pg_ctl restart -l $PGDATA/log -m fast -w -t3 || tail $PGDATA/log
fi
