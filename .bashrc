# ~/.bashrc

## varia #######################################################################
# if working directory isn't accessible, change directory to home
[[ -d "$PWD" ]] || echo "$PWD isn't accessible" 1>&2; cd


## terminal ####################################################################

#in a tty terminal, set beep length to 0 (don't beep!)
test "$TERM" = "linux" && setterm -blength 0


## environment #################################################################

test -f /etc/profile && source /etc/profile

umask 002
export EDITOR="mcedit"
test -x /usr/bin/lv -o -x ~/bin/lv && export PAGER="lv -c"
export LC_ALL="en_US.UTF-8"
export LANG="en_US"
test -d ~/bin && export PATH="$HOME/bin:$PATH"
test -d ~/man && export MANPATH="$HOME/man:$MANPATH"
test "$HOSTNAME" = "ark" && export DISPLAY="172.16.0.1:0.0"
test -x /etc/profile.d/bash-completion && source /etc/profile.d/bash-completion

#set login timeout for the root user (7200 = 2*3600 = 2h)
test "$UID" -eq 0 && TMOUT=7200


## prompt ######################################################################

if [ "$TERM" != "dumb" ]; then
	COL_GREEN="\\[\033[01;32m\\]"
	COL_RED="\\[\033[01;31m\\]"
	COL_NONE="\\[\033[0;0m\\]"
else
	COL_GREEN=""
	COL_RED=""
	COL_NONE=""
fi

if [ "$UID" -eq 0 ]; then
    #the default rootuser prompt; doesn't include username
    PS1="${COL_GREEN}[${COL_RED}\\h${COL_NONE} \\w${COL_GREEN}]\\\$${COL_NONE} "
else
    #the default user prompt
    PS1="${COL_GREEN}[${COL_NONE}\\u@${COL_RED}\\h${COL_NONE} \\w${COL_GREEN}]\\\$${COL_NONE} "
fi

#PS2="${COL_GREEN}[${COL_NONE}...${COL_GREEN}]>${COL_NONE} "
PS2="${COL_GREEN}[${COL_NONE}...${COL_GREEN}]\\\$${COL_NONE} "
unset COL_GREEN COL_RED COL_NONE

#set the xterm title
case "$TERM" in
  screen|xterm|rxvt|rxvt-unicode|xterm-256color)
    PS1="\\[\033]0;bash: \\u@\\h\\\$ \\w\007\\]$PS1"
    ;;
esac


## aliases #####################################################################

alias l="ls -Fh"
alias ll="ls -Fhl"
alias lla="ls -FhAl"
alias llt="ls -rFhAlt"
alias lls="ls -rFhAlS"
alias cd..='cd ..'
alias x="unset HISTFILE"
test -z "$PAGER" && alias lv=less

alias me="mcedit"

alias du0="du -sh"
alias du1="du --max-depth=1 -h"

DIFF_PARAMS="--ignore-space-change --ignore-blank-lines --strip-trailing-cr --expand-tabs --report-identical-files --minimal"
alias diff1="diff $DIFF_PARAMS --new-line-format='+%L' --old-line-format='-%L' --unchanged-line-format=' %L'"
alias diff2="diff $DIFF_PARAMS --new-line-format='+%L' --old-line-format='-%L' --unchanged-line-format=''"
unset DIFF_PARAMS

alias pstree1='watch pstree -A $USER'

#svn aliases
alias svn:ignore='svn propedit svn:ignore'
alias svn:keywords='svn propset svn:keywords Id'
alias svn:mime-type='svn propedit svn:mime-type'


## utility functions ###########################################################

silent() {
    "$@" &> /dev/null &
}


## eof #########################################################################
