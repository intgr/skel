#!/bin/sh
# Script to configure Gnome preferences

# Alt-Tab between windows on current workspace only
gsettings set org.gnome.shell.app-switcher current-workspace-only true
