TARGET=$(HOME)

#### Install targets

INSTALL=.vimrc .inputrc

ifeq (0,$(shell hash zsh &>/dev/null; echo $$?))
INSTALL+=.zshrc .zsh_local
else
INSTALL+=.bashrc
endif

ifeq (0,$(shell hash fish &>/dev/null; echo $$?))
INSTALL+=.config/fish/config.fish .fish_local
endif

ifeq (0,$(shell hash psql &>/dev/null; echo $$?))
INSTALL+=.psqlrc
endif
ifeq (0,$(shell hash git &>/dev/null; echo $$?))
INSTALL+=.gitconfig
INSTALL+=bin/gitclean.sh
endif
ifeq (0,$(shell hash screen &>/dev/null; echo $$?))
INSTALL+=.screenrc
endif
ifeq (0,$(shell hash htop &>/dev/null; echo $$?))
INSTALL+=.config/htop/htoprc
endif

#### Source files

# Always overwrite
SCRIPTS=.vimrc \
	.psqlrc \
	.zshrc \
	.bashrc \
	.screenrc \
	.inputrc \
	.config/fish/config.fish \
	.config/htop/htoprc \
	bin/gitclean.sh

# Don't overwrite if exists
KEEP_SCRIPTS=.zsh_local \
	     .fish_local \
	     .gitconfig \

#### Installation rules

TARGETS=$(addprefix $(TARGET)/, $(SCRIPTS))
$(TARGETS): $(TARGET)/%: %
	mkdir -p $(dir $@) && cp $< $@

KEEP_TARGETS=$(addprefix $(TARGET)/, $(KEEP_SCRIPTS))
$(KEEP_TARGETS): $(TARGET)/%: %
	test -e $@ && touch -c $@ || mkdir -p $(dir $@) && cp $< $@

INSTALL_TARGETS=$(addprefix $(TARGET)/, $(INSTALL))
install: $(INSTALL_TARGETS)

.PHONY: install
.DEFAULT_GOAL = install
