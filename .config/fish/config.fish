#!/usr/bin/env fish
## ~/.config/fish/config.fish #################################################

if [ ! -d . ]
    echo "$PWD isn't accessible" 1>&2
    cd
end

## environment #####################################################################

fish_add_path "$HOME/bin"

if not status is-interactive
    return
end

# Remove greeting text
set fish_greeting

## prompt #####################################################################

if fish_is_root_user
    set -g prompt_char '#'
    set -g prompt_user ""
else
    set -g prompt_char '>'
    set -g prompt_user "$USER@"
end

function fish_prompt
    string join '' -- \
        (set_color brgreen) '[' (set_color normal) \
        $prompt_user \
        (set_color brred) $hostname (set_color normal) \
        (set_color brgreen) "]" $prompt_char (set_color normal) ' '
end

function fish_right_prompt
    string join '' -- \
        (set_color brgreen) '@' (set_color normal) \
        (prompt_pwd --full-length-dirs 2)
end

## tooling ####################################################################

# cmd_prefer cmd1 cmd2 ... -- find the first existing command
function cmd_prefer
    for app in $argv
        if type -q $app
            echo $app
            return 0
        end
    end
    return 1
end

# cmd_set VAR cmd1 cmd2 ...  -- set env VAR if command is found
function cmd_set
    set -l cmd (cmd_prefer $argv[2..-1])
    if [ -n "$cmd" ]
        set -g "$argv[1]" "$cmd"
    end
end

# cmd_alias target cmd1 cmd2 ...
function cmd_alias
    set -l cmd (cmd_prefer $argv[2..-1])
    if [ -n "$cmd" ]
        alias "$argv[1]" "$cmd"
    end
end

## preferred commands #########################################################

cmd_set PAGER less

cmd_set EDITOR vim vi nano
cmd_alias vi   vim vi nano

cmd_set   LS eza    # NOTE: Used by aliases below
cmd_alias ls eza

cmd_alias clip wl-copy pbcopy
cmd_alias open xdg-open
cmd_alias dstat dool

## aliases ####################################################################

if [ "$LS" != "eza" ]
    alias l="ls -Fh"
    alias ll="ls -Fhl"
    alias lla="ls -FhAl"
    alias llt="ls -rFhAlt"
    alias lls="ls -rFhAlS"
else
    alias l="eza"
    alias ll="eza -l"
    alias lla="eza -la"
    alias llt="eza -la --sort=modified"
    alias lls="eza -la --sort=size"
end

# Shell builtins
alias cd..='cd ..'
alias x="set -g fish_private_mode yes"
alias rehash="source ~/.config/fish/config.fish"

alias du1="du -hd1"
alias disasm="objdump -rld"
alias rsync1='rsync -arvP'
alias netstat1='netstat -napt'
alias lsof='lsof -n'
alias dool1='dool --cpu --sys --disk --net -Dsda,sdb,sdc,sdd -N'
alias dstat1='dool1'
alias mtr='mtr --curses'
alias grep='grep --color'
alias mpl='mpv --no-ytdl --playlist'
alias cert='openssl x509 -noout -text -fingerprint -sha256 -certopt no_sigdump'
alias csr='openssl req -noout -text --reqopt no_sigdump'
alias pygeneric='source ~/py/generic/bin/activate.fish'
# git
alias co='git checkout'
alias rebi='git rebi'
alias rebu='git rebu'
alias bra='git bra'

# services
if type -q systemctl
    set -l _SUDO
    if not fish_is_root_user
        set _SUDO 'sudo'
    end
    alias restart="$_SUDO systemctl restart"
    alias reload="$_SUDO systemctl reload"
    alias stop="$_SUDO systemctl stop"
    alias start="$_SUDO systemctl start"
    alias statu="systemctl status"
    alias urestart="systemctl --user restart"
    alias ureload="systemctl --user reload"
    alias ustop="systemctl --user stop"
    alias ustart="systemctl --user start"
    alias ustatu="systemctl --user status"
else if type -q brew
    alias restart="brew services restart"
    alias reload="brew services reload"
    alias stop="brew services stop"
    alias start="brew services start"
    alias statu="brew services list"
end

## functions ###################################################################

function f
    find . -iname '*'$argv[1]'*'
end

## local customizations ########################################################

if [ -f ~/.fish_local ]
    source ~/.fish_local
end

## eof #########################################################################
